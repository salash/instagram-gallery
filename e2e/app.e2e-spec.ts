import { QminderPage } from './app.po';

describe('qminder App', function() {
  let page: QminderPage;

  beforeEach(() => {
    page = new QminderPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
