# Instagram front-end application #
                                                
Developed by: ### Salman Lashkarara ###

The purpose of this project is accessing to the photos of *Instagram* users without having an instagram account or any credential.This front-end communicates with a REST Instagram service API. The communication happens based on the endpoint [https://www.instagram.com/{Username}/media/](Link URL). Unfortunately, this endpoints has a **CORS problem**. We tried to bypass it using *JSONP* get request, but it also does not work.  

Finally, we used [chrome CORS](https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?hl=en) plugin to handle this problem. Therefore, the final user is required to also use this plugin in his system too.    

We used angularjs2 to develop this application. The architect of this application is quite simple. In home  path, we have a form component. This form uses the **InstagramService.ts** to send a http get request for a given username in the form.
In case that, **InstagramService**  provides some data, this data will be considered as a data structure called dataset. Then, we can benefit **dataset** to fill our gallery. If the output of **InstagramService**   request is a 404 error, then the form handles not-found component.
Also, by clicking on each photo, this photo will be presented in single-picture component. This component also present the caption of the picture, and all the comments which other users provide for that picture. To present caption and comments, we used comment component which present user's profile picture and his comment.