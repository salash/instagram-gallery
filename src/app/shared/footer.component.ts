import {Component} from '@angular/core';

@Component({
    selector:'footer',
    template:`
            
            <div class="container" >
                <footer>
                    <div class="row">
                        <div class="col-lg-12">
                            <hr>
                            <p style="margin=0; padding=5px;">Copyright &copy; Developed by Salman Lashkarara Agust 2016</p>
                        </div>
                    </div>
                </footer>
            </div>  
    `
})


export class FooterComponent{


}