import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES} from '@angular/router';

@Component({
    selector:'navbar',
    templateUrl:'app/shared/navbar.component.html',
    directives: [ROUTER_DIRECTIVES] 
})


export class NavbarComponent{

}