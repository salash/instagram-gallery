import {Component} from '@angular/core';
import {GalleryComponent} from './gallery.component';     
import {InstagramService} from './Instagram.service';
import {HTTP_PROVIDERS} from '@angular/http';
import { ActivatedRoute, Params } from '@angular/router'
import {NotFoundComponent} from './not-found.component'

@Component({
    selector: 'form',
    template:`
        <div class="container" style="auto">
        <h2>Receive Instagram posts lively!</h2>
        <h4><p>Enter the name of your favorite celebrity in the bellow box to see all his images in the <strong>instagram</strong>.</p> </h4> 
                    <div class="row">
                            <form  role="form">
                                <div class="col-lg-6">
                                    <div  class="input-group">
                                        <input type="text" class="form-control" [(ngModel)]=username  placeholder="Celebrity Name...">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" (click)="submit()" type="button">Go!</button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                            <div *ngIf=isFound>
                                    <gallery [username]=SentUsername [items]=collectedDataSet></gallery>
                            </div>
                            <div *ngIf=!isFound>
                                <not-found [text]=err></not-found>
                            </div>
                    </div>
        </div>`,
    directives:[GalleryComponent,NotFoundComponent],
    providers:[InstagramService,HTTP_PROVIDERS]
})


export class FormComponent{
   private username: string;
   private SentUsername:string;
   private  collectedDataSet;
   private isFound: boolean;
   private err;

    constructor( private route: ActivatedRoute,private instagramService: InstagramService){

    }
    ngOnInit(){
        this.isFound=true;
        this.err="No picture found for this username";
        this.route.params.forEach(params =>this.username= params['username']);
        if(this.username !==undefined)
            	this.fillGallery();       
    }

    submit(){
            this.fillGallery();     
            this.SentUsername=this.username;
    }
    
    fillGallery(){
        var res=this.instagramService.getGallery(this.username).subscribe(  (result) =>{
                                                                var isFound= (result.items!==undefined) ? false : true;
                                                                localStorage.setItem("username", this.username);
                                                                this.collectedDataSet=result.items;
                                                            },
                                                            (err)=>{
                                                                    this.isFound=false;
                                                                    console.log("Error is:"+err)});

    }

}