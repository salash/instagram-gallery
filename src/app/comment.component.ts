import {Component,Input} from '@angular/core';
import {ROUTER_DIRECTIVES} from  '@angular/router';


@Component({
    selector: 'comment' ,
    template: `    
                    <div class="col-lg-6 col-md-6 col-xs-6 thumb">
                        <div class="post-owner">
                            <div class="media">
                                    <div class="media-left">
                                        <a [routerLink]="['/home/:username', {username: username }]">
                                          <img class="media-object rounder" [src]=imageURI >
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h3 class="media-heading"><strong>{{username}}</strong></h3>
                                                {{comment}}
                                    </div>
                            </div>
                        </div>
                    </div>`,
    directives:[ROUTER_DIRECTIVES],
})

export class CommentCompoent{
    @Input() imageURI;
    @Input() comment;
    @Input() username;

}