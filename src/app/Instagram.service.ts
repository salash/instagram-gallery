import {Http,Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {Injectable} from '@angular/core';
import {dataSet} from './data';

@Injectable()
export class InstagramService {
    
    constructor( private _http:Http ){            
     
    }
    
    getGallery(username: string) : Observable<dataSet> { 

      return  this._http.get("http://www.instagram.com/"+username+"/media/").map(res =>  res.json() )                                                                               

    }
}