import {Component,Input} from '@angular/core';

@Component({
    selector:'not-found',
    template:`
        <div class="container" style="height:500px;">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">{{text}}</h1>
                </div>
            </div>
        </div>        
    `
})


export class NotFoundComponent{
    @Input() text;

}