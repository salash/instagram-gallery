///*
export interface dataSet {

   // status: string;
    items: Item[];
} //*/

interface url {
    url:string
}

 interface Item{
    images: {
        low_resolution:url;
        thumbnail: url;
        standard_resolution:url;
    };
    comments:{
        data:data[];
    } ;
    caption: {
        text:string;
        from:from;
    };
    user:{
        username:string;
        profile_picture:string;
    }

}

interface from{
          username:string;
          profile_picture:string;
}

interface data{
            text:string;
            from:from;    
}