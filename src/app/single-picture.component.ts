import {Component} from '@angular/core';
import {HTTP_PROVIDERS} from '@angular/http';
import { ActivatedRoute, Params } from '@angular/router'
import {CommentCompoent} from './comment.component';
import {InstagramService} from './Instagram.service';
import {NotFoundComponent} from './not-found.component'

@Component({
    selector:'single-picture',
    template:`
    <div class="container">
        <div class="row">
            <div *ngIf="imgageFound" >
                    <div class="col-lg-6 col-md-6 col-xs-6 thumb">    
                                    <div class="thumbnail" href="#">
                                        <img class="img-responsive" [src]=mainPic>
                                    </div>
                    </div>
                    <comment [username]=username   [comment]=caption [imageURI]=profileOwnerPic></comment>
                    <hr>
                    <ul class="custom-list">
                        <li class="item" *ngFor="let follower of followers">
                            <comment [username]=follower.from.username   [comment]=follower.text [imageURI]=follower.from.profile_picture></comment>
                        </li>
                    </ul>
             </div>
            <div *ngIf="!imgageFound"  >
                <not-found [text]=error_text></not-found>
            </div>

        </div>           
     </div>`,
     directives:[CommentCompoent,NotFoundComponent],
     providers:[InstagramService,HTTP_PROVIDERS]
})

export class SinglePictureComponent{
    
    private mainPic;
    private itemNumber;
    private username;
    private profileOwnerPic;
    private caption;
    private followers;
    private imgageFound;
    private error_text;

    constructor( private route: ActivatedRoute,private instagramService:InstagramService){

    }

    ngOnInit(){
        this.route.params.forEach(params => this.itemNumber=params['comment']);

         this.username=localStorage.getItem("username");
         var res=this.instagramService.getGallery(this.username).subscribe(  (result) =>{
                                                                console.log("username: "+ this.username);
                                                                if(result.items[this.itemNumber]!==undefined){
                                                                    this.imgageFound=true;
                                                                    this.mainPic=result.items[this.itemNumber].images.standard_resolution.url;
                                                                    this.profileOwnerPic=result.items[this.itemNumber].user.profile_picture;
                                                                    this.caption=(result.items[this.itemNumber].caption==null) ? '' : result.items[this.itemNumber].caption.text;
                                                                 
                                                                    this.followers=result.items[this.itemNumber].comments.data;    
                                                                }else{
                                                                    this.imgageFound=false;
                                                                    this.error_text='No imgae exist for this user';
                                                                }                                                    
                                                            });
        };
}