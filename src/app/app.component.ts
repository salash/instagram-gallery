import { ROUTER_DIRECTIVES, Router } from '@angular/router';
import { Component } from '@angular/core';
import {NavbarComponent} from './shared/navbar.component';
import {FooterComponent} from './shared/footer.component';

@Component({
  moduleId: module.id,
  selector: 'app-root',
  template: `
        <navbar></navbar>
        <router-outlet></router-outlet>
        <footer></footer>
  `,
  styleUrls: ['app.component.css'],
  directives:[NavbarComponent,ROUTER_DIRECTIVES,FooterComponent]
  
})

export class AppComponent {
 
  }
