import { provideRouter, RouterConfig } from '@angular/router';
import {FormComponent} from './form.component';
import {AboutComponent} from './about.component';
import {SinglePictureComponent} from './single-picture.component';


const routes: RouterConfig = [
    { path:'home/?:username' , component: FormComponent  },
    { path:'single-picture/:comment', component:SinglePictureComponent  },
    { path:'about', component: AboutComponent },
    { path:'**'   , component: FormComponent  }
];

export const appRouterProviders = [
  provideRouter(routes)
]