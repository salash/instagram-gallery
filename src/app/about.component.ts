import {Component} from '@angular/core';
import {CommentCompoent} from './comment.component'


@Component({
    selector:"about",
    template:`
    <div class="container" style=" height:500px;" >
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-xs-6 thumb">
                        <div class="thumbnail" href="#">
                                <img class="img-responsive" [src]=image_pic>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-xs-6 thumb">
                            <p>This application is developed by <strong>Salman Lashkarara. &nbsp;</strong>It is a simple front-end which commiuncates with a REST instagram <strong>service</strong> in the backend.&nbsp;api.The&nbsp;<strong>Angularjs2</strong>&nbsp;is the most important technologies in use.<strong>&nbsp; </strong>To use this application in best way, I do suggest to use chorom and enable it with&nbsp;<a href="https://www.google.ee/url?sa=t&amp;rct=j&amp;q=&amp;esrc=s&amp;source=web&amp;cd=1&amp;cad=rja&amp;uact=8&amp;ved=0ahUKEwjK6Mu-z7nOAhVKECwKHdLCDxMQFggaMAA&amp;url=https%3A%2F%2Fchrome.google.com%2Fwebstore%2Fdetail%2Fallow-control-allow-origi%2Fnlfbmbojpeacfghkpbjhddihlkkiljbi%3Fhl%3Den&amp;usg=AFQjCNHSUFqc6ylxfxfbWzmmFJ6L5QUvyg&amp;sig2=vie1v_A22fV2OdNYJ_AASQ&amp;bvm=bv.129422649,d.bGg">chrome cors plugin</a><strong>&nbsp;.</strong>To see more projects from me please visit <a href="https://www.linkedin.com/in/salman-lashkarara-20013081">my linkdin</a>.</p>
                    </div>
                </div>
    </div>                
    `,directives:[CommentCompoent]
    
})


export class AboutComponent{
    private username;
    private text;
    private image_pic;

    ngOnInit(){
        this.username="Salman Lashkarara";
        
        this.image_pic="app/shared/pic/Main.jpg"

    }

}