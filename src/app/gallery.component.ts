import {Component,Input} from '@angular/core';
import {dataSet} from './data';
import {ROUTER_DIRECTIVES} from '@angular/router';


@Component({
    selector:'gallery',
    template:`
        <div class="container" style="height:500px;">
            <div class="row">

                    <div class="col-lg-12" >
                        <h1 class="page-header">{{username}} Gallery</h1>
                    </div>

                    <ul class="custom-list">  
                                <li *ngFor="let item of items; let i=index" style="display: inline;">    
                                    <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                            <a class="thumbnail" [routerLink]="['/single-picture/:comment', {comment:i} ]">
                                                <img  [src]=item.images.low_resolution.url>
                                            </a>
                                     </div>       
                                </li>  
                                 
                    </ul>           
            </div>
        </div>
    `,
    directives:[ROUTER_DIRECTIVES]
})

export class GalleryComponent{

    @Input() username:string; 
    @Input() items:dataSet;
    
    ngOnInit(){
     
     
    } 

}