import { bootstrap } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { environment } from './app/';
//import {RouteParams} from '@angular/router-deprecated';
//import {RouteParams} from '@angular/router';

import {AppComponent} from './app/app.component';
import {appRouterProviders } from './app/app.routes';


if (environment.production) {
  enableProdMode();
}

bootstrap(AppComponent,[appRouterProviders]);

